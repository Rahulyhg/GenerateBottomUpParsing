The following code is supposed to generate a parse tree of the input expression.
It is clear that Bottom-up parser generation follows the same form as that for top-down generation.
Cherise Orantes - Director at http://astrology.alignedsigns.com